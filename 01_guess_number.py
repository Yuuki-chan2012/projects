# -*- coding: utf-8 -*-

from random import randint

tries = 3
rnd = randint(1, 10)


def check(a: int, b: int):
    global tries

    if a is b:
        tries = 0

        return 'Correct! Thank you for playing.'
    else:
        tries -= 1

        if tries == 0:
            return 'Wrong guess, game over.'
        else:
            return f'Wrong guess, try again. You have {tries} ' + ('tries' if tries > 1 else 'try') + ' remaining.'


if __name__ == '__main__':
    while tries > 0:
        try:
            num = int(input('Enter a number: '))

            if 10 >= num >= 1:
                if tries > 0:
                    print(check(num, rnd))
                else:
                    print('You lost.')
            else:
                print('Please pick a number between 1-10.')
        except ValueError as ve:
            print('That is not a number.')
